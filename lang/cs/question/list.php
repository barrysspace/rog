<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/include/status.inc';

$string['question'] = 'Úloha';
$string['questionbank'] = 'Banka úloh';
$string['notinteam'] = 'Varování: není v týmu';
$string['type'] = 'Typ';
$string['modified'] = 'Upraveno';
$string['status'] = 'Status'; //cognate
$string['myquestionsonly'] = 'pouze moje úlohy';
$string['questionbanktasks'] = 'Správa banky úloh';
$string['currentquestiontasks'] = 'Aktuální úkoly banky úloh';
$string['questionsbytype'] = 'Úlohy podle typu';
$string['questionsbyteam'] = 'Úlohy podle týmů';
$string['questionsbykeyword'] = 'Úlohy podle klíčových slov';
$string['search'] = 'Hledat';
$string['createnewquestion'] = 'Nová úloha';
$string['questions'] = 'Úlohy';
$string['papers'] = 'Dokumenty';
$string['people'] = 'Lidé';
$string['editquestion'] = 'Upravit úlohu';
$string['information'] = 'Informace';
$string['copyontopaperx'] = 'Kopírovat do dokumentu X...';
$string['linktopaperx'] = 'Odkaz na dokument X...';
$string['deletequestion'] = 'Odstranit úlohu';
$string['noquestionleadin'] = 'Varování: v hlavičce není text úlohy!';
$string['owner'] = 'Vlastník';
$string['quickview'] = 'Náhled';
?>