﻿<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/blooms.inc';
require '../lang/' . $language . '/include/status.inc';
require '../lang/' . $language . '/include/question_types.inc';

$string['question'] = 'Úloha';
$string['questions'] = 'Úlohy';
$string['questionsearch'] = 'Hledání úloh';
$string['type'] = 'Typ';
$string['modified'] = 'Upraveno';
$string['wordorphrase'] = 'Slovo nebo fráze';
$string['questiontype'] = 'Typ úlohy';
$string['alltypes'] = '(všechny typy)';
$string['inclockedquestions'] = 'Zahrnout Uzamčené úlohy';
$string['questionsections'] = 'Sekce úloh';
$string['whenmodified'] = 'Datum změny';
$string['metadata'] = 'Metadata'; //cognate
$string['module'] = 'Modul:';
$string['anymodule'] = '(libovolný modul)';
$string['owner'] = 'Vlastník';
$string['anyowner'] = '(libovolný vlastník)';
$string['myquestionsonly'] = '(pouze moje úlohy)';
$string['status'] = 'Status';
$string['keyword'] = 'Klíčové slovo';
$string['back'] = 'Zpět';
$string['search'] = 'Hledat';
$string['editquestion'] = 'Upravit úlohu';
$string['information'] = 'Informace';
$string['copyontopaperx'] = 'Kopírovat do dokumentu X...';
$string['linktopaper'] = 'Odkaz na dokument';
$string['deletequestion'] = 'Odstranit úlohu';
$string['noquestionleadin'] = 'Varování: v hlavičce není text úlohy!';
$string['currentquestiontasks'] = 'Aktuální úkoly banky úloh';
$string['theme'] = 'Téma';
$string['scenario'] = 'Scénář';
$string['leadin'] = 'Popis';
$string['options'] = 'Volby';
$string['dont remember'] = 'Nepamatuji si';
$string['week'] = 'Minulý týden';
$string['month'] = 'Minulý měsíc';
$string['year'] = 'Minulý rok';
$string['specify'] = 'Zadejte datum';
$string['from'] = 'od';
$string['to'] = 'do';
$string['locked'] = 'Uzamčeno';
$string['noquestionsfound'] = 'V rámci zadananých kritérií nebyly nalezeny žádné odpovídající úlohy.';
$string['narrowyoursearch'] = 'Zužte vyhledávání zadáním odpovídajícího výrazu, typu otázky, data změny nebo metadat';
$string['notickedfields'] = 'Nezaškrtli jste žádné pole';
$string['entersearchterm'] = 'Zadejte, prosím, hledaný výraz nebo frázi.';
$string['notickedstatus'] = 'Nezaškrtli jste žádný status';
$string['quickview'] = 'Náhled';
?>