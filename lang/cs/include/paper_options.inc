<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/question_types.inc';
require '../lang/' . $language . '/include/paper_types.inc';

$string['papertasks'] = 'Papírové testy';
$string['testpreview'] = 'Náhled &amp; Testu';
$string['addquestionspaper'] = 'Přidat úlohy do dokumentu';
$string['editproperties'] = 'Upravit vlastnosti';
$string['reports'] = 'Sestavy';
$string['mappedobjectives'] = 'Mapované cíle';
$string['importmarks'] = 'Nahrát známky';
$string['standardssetting'] = 'Standardní nastavení';
$string['importoscemarks'] = 'Import známek OSCE ';
$string['copypaper'] = 'Kopírovat dokument';
$string['deletepaper'] = 'Odstranit dokument';
$string['retirepaper'] = 'Zneplatnit dokument';
$string['printhardcopy'] = 'Tištěná verze';
$string['importexport'] = 'Import/Export'; //cognate
$string['export12'] = 'Export 1.2'; //cognate
$string['exportraf'] = 'Export as Rog&#333; Assessment format';
$string['import'] = 'Importovat';
$string['importraf'] = 'Import Rog&#333; Assessment format';
$string['Continuous'] = 'Jedna plynulá stránka';
$string['Page-break per question'] = 'Zalomení stránky na konci každé úlohy';
$string['currentquestiontasks'] = 'Aktuální úkoly banky úloh';
$string['editquestion'] = 'Upravit úlohu';
$string['previewquestion'] = 'Náhled úlohy';
$string['information'] = 'Informace';
$string['copyontopaperx'] = 'Kopírovat do dokumentu X...';
$string['linktopaper'] = 'Odkaz na dokument';
$string['moveup'] = 'Přesunout výše';
$string['movedown'] = 'Přesunout níže';
$string['changescreenno'] = 'Změnit obrazovku č.';
$string['removefrompaper'] = 'Odstranit z dokumentu';

$string['summativechecklist'] = 'Sumativní kontrolní seznam';
$string['session'] = 'Relace';
$string['mismatch'] = 'Neshoda';
$string['examtime'] = 'Doba zkoušky';
$string['incorrect'] = 'Chybně';
$string['incomplete'] = 'Nedokončeno';
$string['duration'] = 'Trvání';
$string['unset'] = 'Odstranit nastavení';
$string['computerlabs'] = 'Počítačová učebna';
$string['peerreviewed'] = 'Recenzováno';
$string['peerreviewes'] = 'Recenzenti pro vzájemné hodnocení';
$string['externalreviews'] = 'Externí recenzenti';
$string['standardsset'] = 'Nastavení standardů';
$string['mapping'] = 'Mapování';

$string['questionbanktasks'] = 'Správa banky úloh';
$string['questionsbytype'] = 'Úlohy podle typu';
$string['questionsbyteam'] = 'Úlohy podle týmů';
$string['questionsbykeyword'] = 'Úlohy podle klíčových slov';
$string['search'] = 'Hledat';
$string['createnewquestion'] = 'Nová úloha';

$string['questions'] = 'Úlohy';
$string['papers'] = 'Dokumenty';
$string['people'] = 'Lidé';
$string['studentcohort'] = 'Skupina studentů';

$string['dates'] = 'Data';
$string['to'] = 'k/do';
$string['course'] = 'Kurz';
$string['anycourse'] = '&lt;libovolný kurz&gt;';
$string['module'] = 'Modul';
$string['anymodule'] = '&lt;libovolný modul&gt;';
$string['cohort'] = 'Skupina';
$string['allcandidates'] = 'Všichni kandidáti';
$string['top'] = 'Nejlepší';
$string['bottom'] = 'Nejhorší';
$string['year'] = 'Rok';
$string['all'] = 'Vše';
$string['reviews'] = 'Hodnotitelé';
$string['internalpeerreview'] = 'Recenze';
$string['externalexaminers'] = 'Externí recenzent';
$string['sctresponses'] = 'Shoda se scénářem Odpovědi/Důvody';       // script concordance tests
$string['classtotals'] = 'Celkové výkazy třídy';
$string['classtotalsexcel2003'] = 'Celkový výkaz třídy (Excel 2003)';
$string['classtotalscsv'] = 'Celkový výkaz třídy (CSV soubor)';
$string['textboxmarking'] = 'Hodnocení textboxu';
$string['primarymarkbyquestion'] = 'Základní hodnocení dle úloh';
$string['selectpapersforremarking'] = 'Vybrat dokument k opětovnému oznámkování';
$string['secondmarkbyquestion'] = 'Druhotné hodnocení dle úloh';
$string['finalisemarks'] = 'Dokončit hodnocení';
$string['frequencyanalysis'] = 'Frekvenční &amp; diskriminační (U-L) analýza';
$string['learningobjectiveanalysis'] = 'Analýza učebních cílů';
$string['exportresponsescsvnum'] = 'Stáhnout odpovědi jako  CSV soubor (surová data)';
$string['exportresponsescsvtext'] = 'Stáhnout odpovědi jako CSV soubor (text)';
$string['exportbooleancsv'] = 'Stáhnout odpovědi v Booleově algebře jako soubor CSV';
$string['exportmarkscsv'] = 'Stáhnout známky jako CSV soubor';
$string['individualportfoliosheets'] = 'Jednotlivé listy portfolia';
$string['exportratingscsv'] = 'Stáhnout Ohodnocení jako CSV soubor';
$string['ReviewSummary1'] = 'Revidovat přehled - průměr (HTML)';
$string['ReviewSummary2'] = 'Revidovat přehled - procenta (HTML)';
$string['ReviewSummary3'] = 'Revidovat přehled';
$string['ReviewSummary4'] = 'Revidovat přehled - průměr (CSV)';
$string['xhtml'] = 'XHTML'; //cognate
$string['word2003format'] = 'Formát Word 2003';
$string['rawdataxml'] = 'Surová data do XML souboru';
$string['rawdatacsv'] = 'Surová data do CSV souboru';
$string['itemanalysis'] = 'Položková analýza';
$string['cohortreports'] = 'Sestavy skupin';
$string['exports'] = 'Exporty';
$string['quantitativereports'] = 'Kvantitativní sestavy';
$string['qualitativeanalysis'] = 'Kvalitativní analýza';
$string['incabsentcandidates'] = 'vč. chybějících kandidátů';
$string['studentsonly'] = 'pouze pokusy studentů';
$string['completedatasets'] = 'Zobrazit pouze kompletní datové sady ';
$string['anyyear'] = 'Libovolný rok';

$string['copyname'] = 'Kopírovat jméno:';
$string['paperonly'] = 'Pouze dokumenty';
$string['paperandquestions'] = 'Dokument a úlohy';
$string['next'] = 'Další &gt;&gt;';
$string['reorderinfo'] = 'Nyní můžete změnit pořadí úloh a zalomení obrazovky, jejich přetažním na novou pozici';
$string['insertscreenbreak'] = '+ zalomení obrazovky';
$string['deletescreenbreak'] = '- zalomení obrazovky';
$string['addscreenbreak'] = 'Přidat zalomení obrazovky';
$string['midexamclarification'] = 'Ujasnění v průběhu zkoušky';
$string['calculationquestions'] = 'Početní úlohy Questions';
$string['markallquestions'] = 'Označit všechny otázky';
?>