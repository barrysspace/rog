<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['frequencydiscrimination'] = 'Frekvenční & diskriminační analýza';
$string['reporttitle'] = 'Frekvenční &amp; diskriminační (U-L) analýza';
$string['PaperNotAttempted'] = "O tento dokument se nikdo nepokusil.";
$string['NotEnoughData'] = "Nedostatek údajů k výpočtu nejlepší a nejhorší skupiny. Zvolte vyšší procento, prosím.";
$string['AllItemsCorrect'] = "Všechny položky správně";
$string['totalcandidatenumber'] = 'Celkový počet kandidátů';
$string['FullMarks'] = "Celkové známky";
$string['PartialMarks'] = "Částečné známky";
$string['Incorrect'] = "Nesprávně";
$string['Correct'] = "Správně";
$string['True'] = "Pravdivý";
$string['False'] = "Nepravdivý";
$string['TopGroup'] = "Nejlepší skupina";
$string['BottomGroup'] = "Nejhorší skupina";
$string['meanWordCount'] = "průměr počtu slov";
$string['groupsizes'] = 'Horní/spodní velikost skupiny';
$string['pergroup'] = 'za skupinu';
$string['boldstems'] = 'Bold analýza??';
$string['correctanswers'] = 'reprezentuje správné odpovědi';
$string['p_definition'] = 'Obtížnost položky (podíl správných odpovědí studentů)';
$string['d_definition'] = 'hodnota citlivosti';
$string['t_definition'] = 'procento z <strong>všech</strong> položek odpovězených skupinou';
$string['u_definition'] = 'procento z <strong>horních</strong> položek odpovězených skupinou';
$string['l_definition'] = 'procento z <strong>spodních</strong> položek odpovězených skupinou';
$string['warning'] = 'Varování';
$string['p_warning'] = '<strong>p < 0.2</strong> (t.j. velmí těžké)';
$string['d_warning'] = "<strong>d < 0.15</strong> (t.j. slabá)<br />Zjištěna signalizace možného nedostatku,  pokud usoudíte, že je položka špatná, vyloučíte ji z používání pomocí ikony <img src=\"../artwork/exclude_off.gif\" style=\"cursor:pointer\" width=\"23\" height=\"22\" border=\"0\" alt=\"Vyloučit\" /> a klikněte na  'Uložit' ve spodní části okna";
$string['summary'] = 'Přehled';
$string['msg'] = 'Počet položek může být větší než počet úloh, jelikož, kupříkladu, dichotomické otázky, popisky a otázky s výběrem odpovědi, jsou tvořeny z vícero položek a každá z nich může mít vlastní hodnoty citlivosti a obtížnosti.';
$string['difficulty'] = 'Obtížnost';
$string['discrimination'] = 'Citlivost';
$string['noofitems'] = 'Počet položek';
$string['veryeasy'] = 'Velmi snadná';
$string['easy'] = 'Snadná';
$string['moderate'] = 'Průměrná';
$string['hard'] = 'Těžká';
$string['veryhard'] = 'Velmi těžká';
$string['mean'] = 'Průměr';
$string['highest'] = 'Nejvyšší';
$string['high'] = 'Vysoká';
$string['intermediate'] = 'Střední';
$string['low'] = 'Nízká';
$string['save'] = 'Uložit výjimky';
$string['screen'] = 'Obrazovka';
$string['warning1'] = 'Varování: Obtížnost je menší než 0.2';
$string['warning2'] = 'Varování: Rozlišení je menší než 0.15';
?>