<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

include 'lti_common.php';

$string['NoPapers'] = 'K tomuto modulu nejsou dostupné žádné dokumenty';
$string['NoPapersDesc'] = 'K tomuto modulu nejsou dostupné žádné dokumenty. Pravdìpodobnì je to zpùsobeno tím, že jste vytvoøili nový odkaz z VLE s novým modulem, a proto nemáte v souèasnosti nakonfigurované žádné dokumety.<br /><br />K vytvoøení dokumentu vyberte, prosím <a href="../" target="_blank">spustit Rogo</a>'; //zavøete prohlížeè (<strong>velice dùležité</strong>) následnì jdìte na domovskou stránku Rogo a dokument vytvoøte.

$string['NoModCreateTitle'] = 'Vytvoøení nového modulu není povoleno';
$string['NoModCreate'] = 'Tvorba modulu z LTI není povolena v konfiguraci, proto nelze vytvoøit modul s kódem kurzu: ';
$string['NotAddedToModuleTitle'] = 'Pøidání do týmu modulu nebylo úspìšné';
$string['NotAddedToModule'] = 'Pøidávání do týmu modulu není v LTI konfiguraci povoleno, tato závada se vyskytla u modulu: ';

$string['NoModCreateTitle2'] = 'Vytváøení modulu nebìží';
$string['NoModCreate2'] = 'Tvorba modulu z LTI nebìží, jelikož uživatel nevlastní oprávnìní, a proto nelze vytvoøit modul s kódem kurzu: ';

$string['SELECT']='Vybrat';
?>