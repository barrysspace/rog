<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/include/paper_options.inc';
require_once '../lang/' . $language . '/include/paper_types.inc';

$string['showretired'] = 'Pokaż wycofane';
$string['teammembers'] = 'Członkowie zespołu';
$string['calendar'] = 'Kalendarz';
$string['edit'] = 'Edytuj';
$string['screen'] = 'Ekran';
$string['screens'] = 'Ekrany';
$string['mins'] = 'min.';
$string['SelfenrolURL'] = "URL do samodzielnego zapisywania się";
$string['createnewpaper'] = 'Utwórz nowy arkusz';
$string['editpropertiessysadmin'] = 'Edytuj właściwości (SysAdmin)';
$string['manageobjectives'] = 'Zarządzaj celami';
$string['managekeywords'] = 'Zarządzaj słowami kluczowymi';
$string['referencematerial'] = 'Materiał pomocniczy';
$string['importstudentmetadata'] = 'Importuj metadane studentów';
$string['listpapers'] = 'Wyświetl listę arkuszy';
$string['module'] = 'Moduł';
$string['papertasks'] = 'Działania dot. arkuszy';
$string['questionbank'] = 'Bank pytań';
$string['foldertasks'] = 'Działania dot. folderów';
$string['folderproperties'] = 'Właściwości folderu';
$string['makesubfolder'] = 'Utwórz nowy podfolder';
$string['deletefolder'] = 'Usuń ten folder';
$string['questionsbytype'] = 'Pytania wg. typu';
$string['questionsbyteam'] = 'Pytania wg. zespołu';
$string['questionsbykeyword'] = 'Pytania wg. słowa klucz.';
$string['search'] = 'Szukaj';
$string['createnewquestion'] = 'Utwórz nowe pytanie';
$string['questions'] = 'Pytania';
$string['papers'] = 'Arkusze';
$string['people'] = 'Osoby';
$string['type'] = 'Typ';
$string['author'] = 'Autor';
$string['create'] = 'Utwórz';
$string['newfolder'] = 'Nowy Folder';
$string['sudentson'] = 'Studenci w '; 
$string['nomodulesset'] = 'Brak modułów';
?>