<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/paper/finish.php';

$string['nofeedback'] = 'Obecnie nie jest możliwe pokazanie informacji zwrotnych dla tego arkusza.';
$string['nottaken'] = 'Informacje zwrotne nie są dostępne, nie przystąpiłeś/łaś do tego arkusza.';
$string['norights'] = 'Nie masz wystarczających przywilejów aby zobaczyć ten arkusz.';  
$string['msg'] = '<strong>Zasady akademickie</strong><br />1) zakaz opuszczania pomieszczenia egzaminacyjnego przed upływem pierwszej godziny, <br />2) zakaz opuszczania pomieszczenia egzaminacyjnego w czasie ostatnich 15 minut.<br /><br />Jeśli przestrzegane są dwie pierwsze zasady, a egzamin ma tylko jedną turę to można kliknąć na \'Zamknij okno\' a następnie nacisnąć przyciski &lt;CTRL&gt; &lt;ALT&gt; &lt;DELETE&gt; aby wylogować się z tego komputera.'; 
?>