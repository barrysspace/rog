<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

$string['noadd'] = 'No Add';
$string['activepaper'] = 'Warning: Active Paper';
$string['msg'] = 'New questions may not be added to this paper while it is currently active.<br /><br />This is a safety feature to stop papers being accidentally modified while examinees may be taking them.';
$string['solution'] = 'Solution:';
$string['solutionmsg'] = 'Open <a href="#" onclick="paperProperties(); return false;"><img src="../../artwork/small_link.png" width="11" height="11" alt="Shortcut" /></a> <a href="#" style="color:blue" onclick="paperProperties(); return false;">Edit Properties</a> from the \'Current Paper Tasks\' pane and alter the available dates.';
$string['to'] = 'to';
?>