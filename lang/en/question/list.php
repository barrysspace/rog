<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/include/status.inc';

$string['question'] = 'Question';
$string['questionbank'] = 'Question Bank';
$string['notinteam'] = 'Warning: not in module';
$string['type'] = 'Type';
$string['owner'] = 'Owner';
$string['modified'] = 'Modified';
$string['status'] = 'Status';
$string['myquestionsonly'] = 'my questions only';
$string['questionbanktasks'] = 'Question Bank Tasks';
$string['currentquestiontasks'] = 'Current Question Tasks';
$string['questionsbytype'] = 'Questions by Type';
$string['questionsbyteam'] = 'Questions by Module';
$string['questionsbykeyword'] = 'Questions by Keyword';
$string['search'] = 'Search';
$string['createnewquestion'] = 'Create new Question';
$string['questions'] = 'Questions';
$string['papers'] = 'Papers';
$string['people'] = 'People';
$string['quickview'] = 'Quick View';
$string['editquestion'] = 'Edit Question';
$string['information'] = 'Information';
$string['copyontopaperx'] = 'Copy onto Paper X...';
$string['linktopaperx'] = 'Link to Paper X...';
$string['deletequestion'] = 'Delete Question';
$string['noquestionleadin'] = 'WARNING: no question lead-in!';
?>