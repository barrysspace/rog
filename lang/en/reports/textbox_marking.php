<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

$string['textboxmarking'] = 'Textbox Marking';
$string['marks'] = 'marks';
$string['saveexit'] = 'Save &amp; Exit';
$string['nostudents'] = 'No students';
$string['savecontinue'] = 'Save &amp; Continue';
$string['noanswer'] = 'No answer provided';
$string['saveerror'] = 'Error saving mark. Please try again';
$string['hidemarked'] = 'Hide already marked';
$string['primarymarking'] = 'Primary Marking';
$string['secondmarking'] = 'Second Marking';
$string['finalisemarks'] = 'Finalise Marks';
$string['candidates'] = 'Candidates';
$string['screen'] = 'Screen';
$string['note'] = 'Note';
?>