<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

$string['externalexaminerarea'] = 'External Examiner Area';
$string['expired'] = '&lt;expired&gt; - you may still view the paper and read %s actions/reponses to your comments.';
$string['instructions'] = 'Instructions';
$string['externalexamineraccess'] = 'External Examiner Access';
$string['notreviewed'] = 'Not Reviewed';
$string['reviewed'] = 'Reviewed: %s';
$string['msg1'] = 'Below is a list of exam papers requiring review. After clicking on their icons/titles you will be taken to a \'blue\'  summarising information about the exam (this is what student see). To begin a review click \'Start\'. In the paper, beneath each question are three buttons for you to rate the question: 1) <span style="background-color:#C5E0B3; color:#375623">&nbsp;Question&nbsp;OK&nbsp;</span> (Default), 2) <span style="background-color:#FFE599; color:#7F6000">&nbsp;Minor/some&nbsp;problems&nbsp;</span>, or 3) <span style="background-color:#FF9090; color:#800000">&nbsp;Major/several&nbsp;problems&nbsp;</span>. There is also a textbox to directly record your feedback to us where you feel there are points to raise.';
$string['msg2'] = 'Navigation buttons for moving between screens can be found at the bottom of each screen. The correct answers are displayed for all questions in this review mode (students will receive unanswered papers). You may launch a paper any number of times. Comments will be saved automatically when you navigate between screens and click \'Finish\'.';
$string['yourpapersforreview'] = 'Your papers for review include:';
$string['nopapersfound'] = 'No papers found!';
$string['copyrightmsg'] = 'Questions held within Rogō are protected by UK copyright law and are held by %s.';
$string['helpandsupport'] = 'Help and Support';
$string['helpandsupportext'] = 'Help and support for external examiners reviewing papers in Rogō.';
$string['rogodetails'] = 'Rogō %s is an open source e-assessment system lead by Information Services at the University of Nottingham.<br />For further details about Rogō please see the project website:';
$string['onlinesupportsystem'] = 'Online support system for students';
$string['email'] = 'Email';
$string['deadline'] = 'Deadline:';
$string['notset'] = '&lt;not set&gt;';
?>