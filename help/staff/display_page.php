<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

/**
* 
* Displays a page from the Staff online help.
*
* @author Simon Wilkinson
* @version 1.0
* @copyright Copyright (c) 2014 The University of Nottingham
* @package
*/

require '../../include/staff_auth.inc';
require_once '../../include/errors.inc';

function getPath($path, $string) {
  $parts = explode('/',$path);
  $path = '<a style="color:#666666" href="display_page.php?id=1">' . $string['home'] . '</a>';
  if (count($parts) > 1) {
    for ($i=0; $i<count($parts)-1; $i++) {
      $path .= " > <a style=\"color:#666666\" href=\"display_folder.php?title=" . $parts[$i] . "\">" . $parts[$i] . "</a>";
    }
  }
  
  return $path;
}

function getTitle($path) {
  $parts = explode('/', $path);
  
  return $parts[count($parts) - 1];
}

if ($userObject->has_role('SysAdmin')) {
  $roles_check = 'AND roles IN ("SysAdmin", "Admin", "Staff")';
} elseif ($userObject->has_role('Admin')) {
  $roles_check = 'AND roles IN ("Admin", "Staff")';
} else {
  $roles_check = 'AND roles="Staff"';
}

$search_results = $mysqli->prepare("SELECT title, body, type, deleted FROM staff_help WHERE id = ? $roles_check");
$search_results->bind_param('i', $_GET['id']);
$search_results->execute();
$search_results->store_result();
$search_results->bind_result($tmp_title, $tmp_body, $type, $deleted);
while ($search_results->fetch()) {
  $edit_id = $_GET['id'];
  if ($type == 'pointer') {
    $pointer_results = $mysqli->prepare("SELECT title, body, deleted FROM staff_help WHERE id = ?");
    $pointer_results->bind_param('i', $tmp_body);
    $pointer_results->execute();
    $pointer_results->store_result();
    $pointer_results->bind_result($tmp_title, $tmp_body, $deleted);
    $pointer_results->fetch();
    $pointer_results->close();
    $edit_id = $tmp_body;
  }
}
$search_results->free_result();
$search_results->close();

if ($tmp_body == '' and $tmp_title == '') {
  $msg = sprintf($string['furtherassistance'], $configObject->get('support_email'), $configObject->get('support_email'));
  $notice->display_notice_and_exit($mysqli, $string['pagenotfound'], $msg, $string['pagenotfound'], $configObject->get('cfg_root_path') . '/artwork/page_not_found.png', '#C00000');
}

if ($_GET['id'] != '1' and !$userObject->has_role('SysAdmin')) {   // Don't record the homepage or SysAdmin activities.
  $result = $mysqli->prepare("INSERT INTO help_log VALUES (NULL, 'staff', ?, NOW(), ?)");
  $result->bind_param('ii', $userObject->get_user_ID(), $_GET['id']);
  $result->execute();  
  $result->close();
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=<?php echo $configObject->get('cfg_page_charset') ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  
  <title>Help and Support Center</title>
  
  <link rel="stylesheet" type="text/css" href="../../css/body.css" />
  <link rel="stylesheet" type="text/css" href="../../css/staff_help.css" />
  
  <script type="text/javascript" src="../../js/jquery-1.6.1.min.js"></script>
  <script type="text/javascript">
    function updateToolbar(editID, deleteID) {
      var obj = parent.frames[0].document.getElementById('editid');
      if (obj != null) obj.value = editID;
      obj = parent.frames[0].document.getElementById('deleteid');
      if (obj != null) obj.value = deleteID;
      <?php
      if (isset($_GET['section'])) {
        echo "window.location='#" . $_GET['section'] . "'\">\n";
      }
      ?>
    }
    function updateTOC() {
      var obj = parent.frames['navigation'].document.getElementById('old_highlight');
      if (obj != null) {
        if (obj.value != 0) {
          parent.frames['navigation'].document.getElementById(obj.value).style.fontWeight="normal";
        }
        parent.frames['navigation'].document.getElementById('title<?php echo $_GET['id']; ?>').style.fontWeight="bold";
        obj.value = 'title<?php echo $_GET['id']; ?>';
      } else {
        obj2 = parent.frames['navigation'].document.getElementById('title1');
        if (obj2 != null) obj2.style.fontWeight="bold";
      }
    }

   function openTutorial(file) {
     var winheight = screen.height-30;
      var winwidth = screen.width-20;
      notice = window.open("./viewCaptivate.php?tutorial=" + file + "","Tutorial","width=" + winwidth + ",height="+winheight+",scrollbars=yes,resizable=yes,toolbar=no,location=no,directories=no,status=no,menubar=no");
      notice.moveTo(0,0);
      if (window.focus) {
        notice.focus();
      }
   }
   
   $(document).ready(function() {
     updateToolbar(<?php echo $_GET['id']; ?>,<?php echo $_GET['id']; ?>);
     updateTOC();
     });
  </script>
</head>

<?php
  
if ($deleted != '') {
  echo "<body>\n<br />\n<p style=\"margin-left:15px\">" . $string['msg'] . "</p>\n</body>\n</html>\n";
  exit;
}

echo "<body>\n";

if ($_GET['id'] == 1) {
  // ID 1 is for the homepage.
  echo "<div>\n";
} else {
  echo "<a name=\"top\"></a>";
  echo "<div class=\"path\">" . getPath($tmp_title, $string) . "</div>";
  echo "<div style=\"padding:20px; font-size:160%; font-weight:bold; margin-bottom:5px; color:#295AAD\">" . getTitle($tmp_title) . "</div>\n<hr style=\"width:100%; background-color:#B6B6B6; color:#B6B6B6; height:1px; border:0px\" />\n";
  echo "<div style=\"margin-left:20px; margin-right:20px\">\n";
}

$offset = 0;

// Perform replacement on certain strings.
$tmp_body = str_replace('$support_email', '<a href="mailto:' . $configObject->get('support_email') . '">' . $configObject->get('support_email') . '</a>', $tmp_body);

$tmp_body = str_replace('$local_server', NetworkUtils::get_protocol() . $_SERVER['HTTP_HOST'], $tmp_body);

if (isset($_GET['highlight'])) {
  do {
    $found = stripos($tmp_body, $_GET['highlight'], $offset);
    if ($found !== false) {
      $first_part = substr($tmp_body, 0 , $found);
      $open_bracket = strrpos($first_part, '<');
      $close_bracket = strrpos($first_part, '>');
      if (($open_bracket < $found and $found < $close_bracket) or ($close_bracket < $open_bracket)) {
        $offset = $found + strlen($_GET['highlight']);
      } else {
        $tmp_body = substr($tmp_body, 0, $found) . '<span style="background-color:#FFFF00">' . substr($tmp_body, $found, strlen($_GET['highlight'])) . '</span>' . substr($tmp_body, $found + strlen($_GET['highlight']));
        $offset = $found + 48;
      }
    }
  } while ($found !== false);
}
echo $tmp_body;
if ($_GET['id'] > 1) {
  echo "<br clear=\"all\" />\n<hr style=\"width:100%; background-color:#B6B6B6; color:#B6B6B6; height:1px; border:0px; margin-bottom:5px\" />\n</div>\n";
  echo "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width:100%; font-size:90%\"><tr>";
  echo "<td style=\"padding-left:20px\"><a style=\"color:#003366\" href=\"#top\"><img src=\"../../artwork/top_icon.gif\" width=\"9\" height=\"12\" border=\"0\" alt=\"" . $string['top'] . "\" /></a>&nbsp;<a style=\"color:#003366\" href=\"#top\">" . $string['top'] . "</a></td><td style=\"padding-right:20px; text-align:right\">&copy; 2014, The University of Nottingham</td></tr>";
  if ($userObject->has_role('SysAdmin')) {
    echo '<tr><td colspan="2" style="padding-right:20px; text-align:right; color:#316AC5">' . NetworkUtils::get_protocol() . $_SERVER['HTTP_HOST'] . $configObject->get('cfg_root_path') . '/help/staff/index.php?id=' . $_GET['id'] . '</tr>';
  }
  echo "</table>\n";
}
?>
</body>
</html>